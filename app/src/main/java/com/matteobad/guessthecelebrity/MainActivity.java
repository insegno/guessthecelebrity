package com.matteobad.guessthecelebrity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.regex.*;

public class MainActivity extends AppCompatActivity {

    ArrayList<Button> buttons;
    ArrayList<Celebrity> celebrities;
    ImageView celebrityImage;
    Random random;
    SourceDownloaderTask sourceDownloaderTask;
    int correctAnswerPos;

    public void celebrityChosen(View view) {
        Log.i("correct would be", String.valueOf(correctAnswerPos));
        Log.i("user choice", view.getTag().toString());

        if (Integer.valueOf(view.getTag().toString()) == correctAnswerPos) {
            Toast.makeText(this, "Correct!", Toast.LENGTH_SHORT).show();
        } else {
            String message = String.valueOf("Wrong! It was " + buttons.get(correctAnswerPos).getText());
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }

        this.nextQuestion();
    }

    private void nextQuestion() {
        int correctAnswerIndex = random.nextInt(100);
        correctAnswerPos = random.nextInt(4);

        for (int i = 0; i < 4; i++) {
            if (i == correctAnswerPos) {
                try {
                    Celebrity celebrity = celebrities.get(correctAnswerIndex);
                    String nomeCelebrita = celebrity.getName();
                    buttons.get(i).setText(nomeCelebrita);
                    celebrityImage.setImageBitmap(celebrities.get(correctAnswerIndex).getPhoto());
                    Log.i("correct button", String.valueOf(i));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                int wrongAnswerIndex = random.nextInt(100);
                buttons.get(i).setText(celebrities.get(wrongAnswerIndex).getName());
                Log.i("wrong button", String.valueOf(i));
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize buttons
        buttons = new ArrayList<>();
        buttons.add((Button) findViewById(R.id.option1));
        buttons.add((Button) findViewById(R.id.option2));
        buttons.add((Button) findViewById(R.id.option3));
        buttons.add((Button) findViewById(R.id.option4));

        celebrities = new ArrayList<>();
        random = new Random();
        sourceDownloaderTask = new SourceDownloaderTask();

        try {
            String source = sourceDownloaderTask.execute("http://www.posh24.se/kandisar").get();

            String[] splitSource = source.split("div class=\"sidebarContainer\">");
            Pattern patternImg = Pattern.compile("img src=\"(.*?)\"");
            Pattern patternName = Pattern.compile("alt=\"(.*?)\"");
            Matcher matcherImg = patternImg.matcher(splitSource[0]);
            Matcher matcherName = patternName.matcher(splitSource[0]);

            while (matcherImg.find() && matcherName.find()) {
                Log.i("group", matcherImg.group(1));
                Celebrity celebrity = new Celebrity(matcherName.group(), matcherImg.group(1));
                celebrities.add(celebrity);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        celebrityImage = (ImageView) findViewById(R.id.imageView);
        this.nextQuestion();
    }
}

