package com.matteobad.guessthecelebrity;


import android.graphics.Bitmap;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class Celebrity {

    private CelebrityPhotoDownloaderTask celebrityPhotoDownloaderTask;
    private String name;
    private String photoAddress;

    public Celebrity(String name, String photoAddress) {
        celebrityPhotoDownloaderTask = new CelebrityPhotoDownloaderTask();
        this.name = name;
        this.photoAddress = photoAddress;
    }

    public String getName() {
        return name;
    }

    public Bitmap getPhoto() throws ExecutionException, InterruptedException {
        return celebrityPhotoDownloaderTask.execute(this.photoAddress).get();
    }
}
